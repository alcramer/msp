# Copyright 2011 Al Cramer

from defs import *
import vcb
import pg
import pgmatch
from pgmatch import match_re,matS,matE,pgRE,decl_re
import srSecondary
import roles

def get_sr_region(e):
    """
    Get next region of graph, starting at "e", over which we
    establish syntax relations.
    """
    # conjunctions and puctuation delimit the regions
    delim = WP_punct|WP_conj
    while e is not None:
        # conjunctions and puctuation delimit the regions
        if e.check_prop(delim):
            e = e.nxt
            continue
        terms = [e]
        # extend to next delimitor
        while e.nxt is not None:
            if e.nxt.check_prop(delim):
                break
            terms.append(e.nxt)
            e = e.nxt
        return terms
    return None

# establish prep <-> verb bindings
def bind_prep(e):
    # e is the prep to be bound
    if not e.check_prop(WP_prep):
        return e.prv
    ep = e
    prep = ep.get_wrd(0)
    v0,fit0 = None,-1
    v1,fit1 = None,-1
    ex = ep.prv
    # Walk left, to a max of 2 verbs; punctuation and
    # conjunctions terminate the walk.
    while ex != None:
        if ex.check_prop(WP_conj|WP_punct):
            break
        if ex.is_verb():
            if v0 is None:
                v0 = ex
                fit0 = vcb.get_prep_verb_fitness(prep,v0.get_vroot())
            elif v1 is None:
                v1 = ex
                fit1 = vcb.get_prep_verb_fitness(prep,v1.get_vroot())
                break
        ex  = ex.prv
    if fit1 > fit0:
        v1.verb_to_prep = ep
        ep.prep_to_verb = v1
        return v1.prv
    return e.prv

# utility: set scope for each term in a match result
def set_scope_terms(ix_term,v,rel):
    for e in pgRE.match_result[ix_term-1]:
        e.set_scope(v,rel)

def set_sv_relations(e):
        """ Set subject-verb relations. """
        if match_re("Query Inf",e):
            # "when to go" translates as Q term w. no subject
            v = matS(2)
            matS(1).set_scope(v,SR_isqby)
            v.set_g(G_nvexpr)
            return v.nxt
        if match_re("Root Be",e):
            # "Hope is a nice girl"
            v = matS(2)
            ag = matS(1)
            ag.vprops = 0
            ag.verbs = []
            ag.props = WP_x
            ag.set_scope(v,SR_agent)
            v.set_g(G_subverb|G_agentaction)
            return v.nxt
        if match_re("Nexpr Mod!V* V",e):
            # the general case
            v = matS(3)
            subS = matS(1)
            subE = matE(1)
            # If the term is marked as a verb, unmark it: we take it
            # to be a verb in a noun role.
            ex = subS
            while True:
                if ex.check_prop(WP_verb):
                    ex.vprops = 0
                    ex.verbs = []
                    ex.props = WP_x
                ex.set_scope(v,SR_agent)
                if ex == subE:
                    break
                ex = ex.nxt
            set_scope_terms(2,v,SR_undef)
            # set the grammar attribute. All cases are marked
            # "SubVerb". Additional markings are:
            # subject-gerund is NVexpr;
            # subject-participle is NVexpr|AgentAction
            #   (cases: "the strongest rain ever recorded", "the
            #     press reported")
            # agent action for everything else
            v.set_g(G_subverb)
            if v.check_vp(VP_gerund):
                v.set_g(G_nvexpr)
            elif v.check_vp(VP_participle):
                v.set_g(G_nvexpr|G_agentaction)
            else:
                v.set_g(G_agentaction)
            # agent-action that starts with a clause-prep is marked
            # as a subordinate clause ("after we go...")
            if subS.prv is not None and \
                subS.prv.check_prop(WP_clprep) and \
                v.check_g(G_agentaction):
                v.set_g(G_subordcl)
            # If the subject CANNOT be an object, we say the
            # verb's subject is immutable. Look for subject pronoun:
            # I you we he she they it
            # (exclude "you" and "it" -- these are context
            # independent).
            test = vcb.spell(subS.wrds[0]).lower()
            if test in ["i","we","he","she","they"]:
                v.set_g(G_subimmutable)
            return v.nxt
        return e.nxt
    
def is_sub(e):
    """ helper for "set_q_relations": is "e" a subject? """
    return e is not None and \
        e.scope is not None and \
        e in e.scope.rel[SR_agent]

def set_q_relations(e):
        """ set Q relations """
        # Some matches cases below involve "that" ("the girl that
        # you saw"). Start by skipping cases where "that" functions
        # as a clause-head.
        if match_re("V Nexpr _that AgentAction",e):
            # "I saw today that the code was wrong"
            return matS(4).nxt
        if match_re("V PrepProps Nexpr Nexpr? _that AgentAction",e):
            # "It occurred to me (today) that the code was wrong"
            return matS(6).nxt
        # Now the accept cases
        if match_re("Nexpr _of|_with Ger",e):
            # "the trouble of getting and picking the daisies"
            v = matS(3)
            matS(1).set_scope(v,SR_isqby)
            set_scope_terms(2,v,SR_undef)
            v.set_g(G_nvexpr)                    
            return v.nxt
        if match_re("Nexpr Adj Inf",e):
            # "a cake good enough to eat"
            # Adj is assigned a subject (agent) role; Nexpr is
            # qulified by verb.
            v = matS(3)
            adj = matS(2)
            adj.set_scope(v,SR_agent)
            e.set_scope(v,SR_isqby)
            v.set_g(G_nvexpr)                    
            return v.nxt
        if match_re("Think|Feel _it Mod|Nexpr Inf",e):
            # "I think it (sad|a pity) to see ..."
            # "it" becomes part of the verb phrase; $3 is qualified
            # by the Inf
            matS(2).set_scope(matS(1),SR_undef)
            q = matS(3)
            inf = matS(4)
            q.set_scope(inf,SR_isqby)
            return inf.nxt
        if match_re("Mod Inf",e):
            # TODO: it was a pity to see
            # "It was too hard to see"
            # The infinitive qualifies the Mod. Note this is not
            # marked G_nvexpr: general case is "It was strange to
            # hear him say the mass". 
            v = matS(2)
            e.set_scope(v,SR_isqby)                   
            return v.nxt
        if match_re("Nexpr _who|_that|_which  V",e):
            # "the man who said..."
            S = matS(0)
            if S.check_prop(WP_verb) or is_sub(S):
                # verb, or subject of a verb: cannot be qualified
                return S.nxt
            v = matS(3)
            matS(1).set_scope(v,SR_isqby)
            set_scope_terms(2,v,SR_undef)
            v.set_g(G_nvexpr|G_qimmutable)
            return v.nxt
        if match_re("Nexpr QualPrep? _who|_whom|_that|_which?  AgentAction",e):
            S = matS(0)
            if S.check_prop(WP_verb) or is_sub(S):
                # verb, or subject of a verb: cannot be qualified
                return S.nxt
            if S.check_prop(WP_prep|WP_clprep):
                # "... said that <verb phrase>":
                # accept "S" as a prep, not a qualified term
                return S.nxt
            if matS(2) is not None and matS(3)is None:
                # reject: qual prep is there to modify term#3,
                # and term#3 is null
                return S.nxt
            v = matS(4)
            matS(1).set_scope(v,SR_isqby)
            set_scope_terms(2,v,SR_undef)
            set_scope_terms(3,v,SR_undef)
            v.set_g(G_nvexpr)
            if matS(3) is not None:
                v.set_g(G_qimmutable)    
            return v.nxt
        return e.nxt

def mark_action_names():
    """
    Action names are verb expressions that can function as
    nouns. They are typically gerunds ("going to the moon") or
    infinitives ("to go to the moon".
    """
    e = pg.eS
    decl_re("%objTerm","[Prep Nexpr]|Nexpr")
    while e != None:
        # Action names: Gerund forms
        if match_re("Ger _that Nexpr V Nexpr|Mod|Prep* V",e):
            # "[Hoping that the Americans leave] is useless".
            v = matS(1);
            v.set_g(G_actname|G_nvexpr)
            e = matE(1).nxt
            continue
        if match_re("Ger Nexpr V Nexpr|Mod|Prep* Be",e):
            # "[Hoping the Americans leave] is useless"
            v = matS(1)
            v.set_g(G_actname|G_nvexpr)
            e = matE(1).nxt
            continue
        if match_re("Ger Inf Nexpr|Mod|Prep* V",e):
            # "[Planning to leave on Friday] is stupid".
            v = matS(1)
            v.set_g(G_actname|G_nvexpr)
            e = matE(1).nxt
            continue
        if match_re("Ger Nexpr|Mod|Prep* V",e):
            # "[Killing them] won't work"
            v = matS(1)
            v.set_g(G_actname|G_nvexpr)
            e = matE(1).nxt
            continue
        # Action names: infinitive forms.
        # Infinitive cases are even more restrictive. Usually it
        # opens a strong left scope:
        # "I want to leave Paris after I've finished my book"
        # But sometimes it functions as an action name:
        # "To leave Paris would be stupid".
        # We accept a very limited number of forms and require
        # an explicit "Be" after the form.
        if match_re("Inf _that? Nexpr V Nexpr|Mod|Prep* Be",e):
            # "[To hope that? the Americans leave] is stupid
            v = matS(1);
            v.set_g(G_actname|G_nvexpr)
            e = matE(1).nxt
            continue
        if match_re("Inf Inf Nexpr|Mod|Prep* Be",e):
            # "[To want to leave now] is stupid"
            v = matS(1);
            v.set_g(G_actname|G_nvexpr)
            e = matE(1).nxt
            continue
        if match_re("Inf Nexpr|Mod|Prep* Be",e):
            # "[To leave now] is stupid"
            v = matS(1);
            v.set_g(G_actname|G_nvexpr)
            e = matE(1).nxt
            continue
        e = e.nxt

def set_syn_relations():
    """
    Set syntax relations
    """
    # trace_parse = trace_parse
    # set syntax relations across verb domains
    terms = get_sr_region(pg.eS)
    while terms is not None:
        save_pg = pg.reset_span(terms[0],terms[-1])

        # set verb/prep bindings
        pg.walk_rtol(bind_prep)
        # left verb domain relations
        pg.walk(set_sv_relations)
        pg.walk(set_q_relations)
        if trace_parse:
            pg.printme(None,"setLeftVdom")
        # mark verb expressions that can function as names
        mark_action_names()
        if trace_parse:
            pg.printme(None,"markActionNames")    
        # set syntax relations in the entangled
        # verb domains.
        srSecondary.parse()
        if trace_parse:
            pg.printme(None,"post SrSecondary")
	# set thematic roles
        roles.set_roles()

        pg.restore_span(save_pg)
        terms = get_sr_region(terms[-1].nxt)


