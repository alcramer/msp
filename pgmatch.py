# Copyright 2011 Al Cramer

""" Syntax functions used by the parser """

from defs import *
import vcb
import lexer
import re
from rematch import ReMatch,wpdct

class PgRE(ReMatch):
    """
    regular expression matching for the parser: match node sequences
    in the parse-graph against a regular expression.
    """
    def __init__(self):
        ReMatch.__init__(self)
        
    def update_state(self,state,consumed):
        # "state" means: the next available node for matching
        if consumed is None or len(consumed) == 0:
            # nothing was concerned in the last match: state
            # does not change
            return state
        else:
            e_last = consumed[len(consumed)-1]
            return e_last.prv if mat_reverse else e_last.nxt

    def match_func(self,func_spec,e):
        """ match using a function """
        if func_spec == 'Be':
            return [e] if e.test_vroot('be') else None
        elif func_spec == 'Have':
            return [e] if e.test_vroot('have') else None
        elif func_spec == 'Do':
            return [e] if e.test_vroot('do') else None
        elif func_spec == 'Get':
            return [e] if e.test_vroot('get') else None
        elif func_spec == 'Think':
            return [e] if e.test_vroot('think') else None
        elif func_spec == 'Feel':
            return [e] if e.test_vroot('feel') else None
        elif func_spec == 'TickS':
            return [e] if e.check_prop(WP_ticks) else None
        elif func_spec == 'Comma':
            return [e] if e.test_wrd(',') else None
        elif func_spec == 'Ger!Be':
            if e.check_vp(VP_gerund) and not e.test_vroot('be'):
	            return [e]
            return None
        elif func_spec == 'Mod!V':
            if e.check_prop(WP_mod):
                if not e.check_prop(WP_verb):
                    return [e]
            return None
        elif func_spec == 'Noun!V':
            if e.check_prop(WP_noun) and not e.check_prop(WP_verb):
                return [e]
            return None
        elif func_spec == 'Adv!V':
            if e.check_prop(WP_adv):
                if not e.check_prop(WP_verb):
                    return [e]
            return None
        elif func_spec == 'Adj!V':
            if e.check_prop(WP_adj):
                if not e.check_prop(WP_verb):
                    return [e]
            return None
        elif func_spec == 'QualPrep!_to':
            if e.check_prop(WP_qualprep):
                if not e.test_wrd('to'):
                    return [e]
            return None
        elif func_spec == 'Mod!V!ClMod':
            if e.check_prop(WP_mod):
                if not e.check_prop(WP_verb|WP_clmod):
                    return [e]
            return None
        elif func_spec == 'V!Ger':
            if e.check_prop(WP_verb) and not e.check_vp(VP_gerund):
                return [e]
            return None
        elif func_spec == 'V!Ger!Past':
            if e.check_prop(WP_verb) and not e.check_vp(VP_gerund|VP_past):
                return [e]
            return None
        elif func_spec == 'ModV':
            if e.check_prop(WP_mod) and e.check_prop(WP_verb):
                return [e]
            return None
        elif func_spec == 'NounV':
            if e.check_prop(WP_noun) and e.check_prop(WP_verb):
                return [e]
            return None
        elif func_spec == 'V!evt':
            if e.check_prop(WP_verb) and not e.test_verb_form(VP_evt):
                return [e]
            return None
        elif func_spec == 'Be!Ger!Inf':
            if e.check_prop(WP_verb) and \
                e.test_vroot('be') and \
                not e.test_verb_form(VP_gerund|VP_inf):
                return [e]
            return None
        elif func_spec == 'VAdj!Ger!Inf':
            # "have" and "do" verbs are allowed as VAdj
            if e.check_prop(WP_verb) and \
                not e.test_verb_form(VP_gerund|VP_inf):
                if e.test_verb_form(VP_adj) or \
                    e.test_vroot('do') or \
                    e.test_vroot('have'):
                    return [e]
            return None
        elif func_spec == 'PartEvt':
            if e.check_prop(WP_verb) and \
                e.check_vp(VP_participle) and \
	            not e.test_verb_form(VP_evt):
                return [e]
            return None
        elif func_spec == 'Nexpr':
            if not e.check_prop(WP_verb):
                if e.check_prop(wpdct.get('NProps')):  
                    return [e]
            return None
        elif func_spec == 'SubPro':
            # Subject pronoun. These are: I you we he she they it
            # Here we want subject pronouns which are context
            # independant; so we exclude "you" and "it", since they can
            # be object pronouns (depends of context).
            test = vcb.spell(e.wrds[0]).lower()
            if test in ["i","we","he","she","they"]:
                return [e]
            return None
    
        elif func_spec == 'DetPhrDelimLeft':
            # a term that that functions as the left context
            # of a det phrase (but is not part of the phrase).
            # Needed for phrases that start with a eak determinant.
            if e.check_prop(WP_verb|WP_prep|WP_conj|WP_punct):
                return [e]
            return None
    
        elif func_spec == 'DetPhrDelimRight':
            # a term that that functions as the right context
            # of a det phrase (but is not part of the phrase).
            if e.check_prop(WP_verb|WP_prep|WP_conj|WP_punct):
                return [e]
            test = vcb.spell(e.wrds[0]).lower()
            if test in ["who","which","that"]:
                # "the man WHO sold the moon"
                return [e]
            return None
    
        elif func_spec == 'DetPhrMod':
            # a term that appears a modifier in a det-phrase
            if e.check_prop(WP_mod|WP_num|WP_x) or e.check_vp(VP_gerund):
                return [e]
            return None

        elif func_spec == 'DetPhrRoot':
            # the root in a det-phrase. Verbs other than gerunds
            # are excluded: "initialPass" is responsible for handling
            # the "Det V" cases.
            if e.is_verb() and not e.check_vp(VP_gerund):
                return None
            # exclude certain cases
            # MUSTDO: initial phase handling for "DetS can"
            if e.check_vp(VP_adj):
                return None
            if e.test_wrd('that'):
                return None
	    # Mod is for "the best".
	    # DetW is for "the one"
	    # Gerund is for "a curious feeling"
            if e.check_prop(WP_x|WP_noun|WP_mod) or \
	            e.check_prop(WP_detw) or \
                e.check_vp(VP_gerund):
                return [e]
            return None
        elif func_spec == 'DetPhrRoot!Ger':
            # above case, excluding gerund
            # (we want "a curious feeling" to parse as a det phr, but
            # "half hoping" to be verb)
            if e.check_vp(VP_gerund):
                return None
            return match_func('DetPhrRoot',e)
    
        elif func_spec == 'SVHead':
            # first term in subject-verb. We accept any term explicitly
            # marked "Sub", plus any Nexpr terms.
            if e.sc == vcb.lkup_sc('Sub') or \
                e.check_prop(wpdct.get('Nexpr')):   
                return [e]
            return None
        # tests on verb-phrases. We return the verb node. These assume
        # subject and Q relations were set, and verbs were marked.
        elif func_spec == 'AgentAction':
            # subject followed by appropriately marked verb
            if e.scope is not None and \
                e.sr == SR_agent and \
                e.scope.check_g(G_agentaction):
                return[e.scope]
            return None
        elif func_spec == 'NVexpr':
            # a verb expression acting as a noun
            # subject-gerund: "the girl sitting over there"
            if e.scope is not None and \
                e.sr == SR_agent and \
                e.scope.check_g(G_nvexpr):
                return[e.scope]
            # infinitives and gerunds by themselves
            if e.is_verb() and \
                e.check_g(G_nvexpr):
                return [e]
            return None
        elif func_spec == 'QSubVerb':
            # qualified term, followed by subject and verb
            if e.scope is not None and \
                e.sr == SR_isqby and \
                e.scope.check_g(G_subverb):
                return [e.scope]
            return None
        elif func_spec == 'SubVerb':
            # subject followed by verb
            if e.scope is not None and e.sr == SR_agent:
                return[e.scope]
            return None
    
        else:
            assert False, func_spec

# our reg.expression match engine; and match context info.
# (Context info is passed into "matchRe" as a parameter, then
# saved as a global for use by the call-back match delegate
# function).
pgRE = PgRE()
mat_context = None
mat_reverse = False

def decl_re(reName,_re):
    pgRE.decl_re(reName,_re)

def set_cache_enabled(v):
    pgRE.set_cache_enabled(v)

def match_re(_re,e,_mat_context=None):
    global mat_context
    mat_context = _mat_context
    return pgRE.match(e,_re,e)

def match_reverse(_re,e):
    global mat_reverse
    mat_reverse = True
    result = match_re(_re,e)
    mat_reverse = False
    return result

def matS(i):
    """
    get first node in set of nodes that match to i_th term in a
    matched regular expression. By convention, terms are indexed
    1..N; "0" means the entire expression.
    """
    if i == 0:
        for term in pgRE.match_result:
            if len(term) > 0:
                return term[0]
    else:
        term = pgRE.match_result[i-1]
        if len(term) > 0:
             return term[0]
    return None

def matE(i):
    """
    get last node in set of nodes that match to i_th term in a
    matched regular expression. By convention, terms are indexed
    1..N; "0" means the entire expression.
    """
    if i == 0:
        i = len(pgRE.match_result)-1
        while i >= 0:
            term = pgRE.match_result[i]
            if len(term) > 0:
                return term[len(term)-1]
            i -= 1
    else:
        term = pgRE.match_result[i-1]
        if len(term) > 0:
             return term[len(term)-1]
    return None

def mat_terms():
    """
    get all terms in a match
    """
    nds = []
    e = matS(0)
    while True:
        nds.append(e)
        if e == matE(0):
            break
        e = e.nxt
    return nds
