import re
from defs import *
import vcb
import pg
from rematch import ReMatch

"""
Here are the rules for thematic roles. A rule is a sequence
of numbered terms, followed by an optional "sub" term.
Term#0 gives the properties of the verb: if a verb satisfies
these props, then the rule applies. Remaining terms describe
the object terms, and gives the roles for the terms. The optional
"sub" term at the end gives the role for words that are in a
(gramatical) subject-relation to the verb. Absent an explicit
"sub" term, we assign the role appropriate to the verb kind
(ave, evt, etc.)
"""
rules_src = \
"""

###########################################################
# Q cases
###########################################################

# AVGT cases

# passive: the apple you were given today
0. V:Q:avgt:passive
1. .* (qual)
sub (auxTheme)

# active: the apple I gave (to) you today
0. V:Q:avgt
1. objPrep? (auxTheme)
2. objTerm (auxTheme)
3. .* (qual)

# default: qualified term is the theme, so object terms are
# modifiers.
0. V:Q:avgt
1. .* (qual)
sub (agent)

# passive cases. These include:
# AVE: "the girl who was delighted [by the gift]"
# EVT: "the boy who was seen [by millions]"
# AVT: "the pie that was baked [by Sally]"

# the pie that was made by Sally
0. V:Q:passive
1. [_by .*] (sub)

# default Q/passive
0. V:Q:passive
1. .* (qual) 

# null subject translated as Q: (it was) hard to see her
0. V:Q:noSubject
1. .* (theme)

# Query Q case: "where can you get clams around here"
0. V:Q:isqquery
1. .* (theme)
sub (sub)

# active Q case: the girl I saw today
0. V:Q
1. .* (qual)
sub (sub)

###########################################################
# passive cases (Q:passive handled above)
###########################################################

# the message was given to me
0. V:passive:avgt
1. objPrep (auxTheme)
2. objTerm (auxTheme)
3. .* (qual)
sub (topic)

# I was given a message today
0. V:passive:avgt
1. objTerm (theme)
2. .* (qual)
sub (topic)

# default avgt passive: I was taken to jail
0. V:passive:avgt
1. .* (qual)
sub (topic)

# "She was delighted by the gift": exper AVE _by agent
0. V:passive:ave
1. [_by objTerm] (agent)
2. .* (qual)
sub (exper)

# "I am not interested in money": exper AVE theme
0. V:passive:ave
2. .* (theme)
sub (exper)

# "They were seen by millions": exper EVT _by agent
0. V:passive:evt
1. [_by objTerm] (exper)
2. .* (qual)
sub (topic)

# default passive "by": the cake was eaten _by me
0. V:passive
1. [_by objTerm] (agent)
2. .* (qual)
sub (topic)

# default passive form: topic V theme
0. V:passive
1. .* (theme)
sub (topic)

###########################################################
# Active cases
###########################################################

# I gave an apple to her on Monday
0. V:avgt
1. objTerm (theme)
2. [objPrep objTerm] (auxTheme)
3. .* (qual) 
sub (agent)

# I gave her an apple
0. V:avgt
1. objTerm (auxTheme)
2. objTerm (theme)
3. .* (qual) 
sub (agent)

# Following rules are for unmodified active verbs. Here we
# can break the object list up into theme + qual.
0. V:notModified
1. %immedObjTerm (theme)
2. [Prep objPronoun] (qual)
3. .* (qual)
sub (sub)

# "shedding gallons of tears": the second verb is actually X
0. V:notModified
1. X (obj)
2. [qualPrep V] (obj)
3. .* (obj)
sub (sub)

0. V:notModified
1. %immedObjTerm (theme)
2. objPrep (qual)
3. .* (qual)
sub (sub)

0. V:notModified
1. %immedObjTerm (theme)
2. objTerm (qual)
3. .* (qual)
sub (sub)

0. V:notModified
1. [Prep X] (obj)
2. .* (qual)
sub (sub)

# Verb + infinitive cases
# "use rice to make beer"
0. V
1. objTerm (theme)
2. inf (qual)
3. .* (qual)
sub (sub)

# "try to sell the goods"
0. V
1. inf (theme)
2. .* (qual)
sub (sub)

# standard cases for active verbs: use preps and mods to
# distinguish bewteen obj and qual

# "I saw today that the code was wrong"
0. V
1. objTerm (qual)
2. _that (theme)
3. .* (theme)
sub (sub)

# "It occured to me (today) that the code was wrong"
# Here we use the "auxTheme" relation: the model parallels
# the AVGT construct
0. V
1. Prep (auxTheme)
2. objTerm (auxTheme)
3. objTerm? (auxTheme)
4. _that (theme)
5. .* (theme)
sub (sub)

# "to walk home during the night"
0. V
1. .+ (obj)
2. clPrep (qual)
3. .* (qual)
sub (sub)

# "to walk home today"
0. V
1. .+ (obj)
2. clMod (qual)
3. .* (qual)
sub (sub)

# final default
0. V
1. .* (obj)
sub (sub)

"""
class RoleRE(ReMatch):
    """
    Regular expression machinary for role processing: match list of Pn
    (parse nodes) against a regular expression.
    """
    def __init__(self):
        ReMatch.__init__(self)
        self.verb = None
        self.src = None
        self.decl_re("%qualObjTerm","X Prep X")
        self.decl_re("%immedObjTerm","[%qualObjTerm|X]")

    def set_source(self,_verb,_src):
        self.verb = _verb
        self.src = _src

    def match_func(self,re_term,term):
        if re_term == "objTerm":
            # anything except a prep
            if not term.check_prop(WP_prep):
                return [term]
            return None
        if re_term == "objPronoun":
            # an object pronoun
            if term.test_wrd(\
                ["me","you","us","him","her","them","it"]) :
                return [term]
            return None
        if re_term == "Prep":
            # any kind of prep
            if term.check_prop(WP_prep|WP_clprep|WP_qualprep):
                return [term]
            return None
        if re_term == "qualPrep":
            # qualifying prep
            if term.check_prop(WP_qualprep):
                return [term]
            return None
        if re_term == 'objPrep':
            # object prep associated with the verb
            if vcb.get_prep_verb_fitness(\
                term.get_wrd(0),self.verb.get_vroot()) >0 :
                return [term]
            return None
        if re_term == 'clPrep':
            # prep at head of clause: "during the night"
            if term.check_prop(WP_clprep):
                return [term]
            return None
        if re_term == 'clMod':
            # clause modfier: "we left [yesterday]"
            if term.check_prop(WP_clmod):
                return [term]
            return None
        if re_term == "X":
            # unown vocab word
            if term.check_prop(WP_x):
                return [term]
            return None
        
        if re_term == "inf":
	    # infinitive
            if term.check_vp(VP_inf):
                return [term]
            return None

        if re_term == "V":
	    # any verb
            if term.is_verb():
                return [term]
            return None

        # next line is dev code
        assert False, 'debug1: %s' % re_term
        return None

roleRE = RoleRE()

class RoleRule:
    """ a compiled rule for thematic roles """
    def __init__(self,ix,v_spec):
        # index is useful in debugging.
        self.ix = ix
        # verb's specifier
        self.v_spec = v_spec
        # reg.expr for the rule's object terms
        self.reObj = ''
        # sequence of (syntax) relations for object terms: there's
        # element for each term in the re.
        self.sr_obj = []
        # This sr applies to terms in a subject relation to
        # the verb. Default is 0xff, meaning undefined.
        self.sr_sub = 0xff
        # source text for rule (debugging aid)

    def tostr(self):
        def rel_tostr(sr):
            if sr == 0xff:
                return '0xff'
            return SRids[sr]
        tmp = ['rule%d.' % self.ix]
        v_desc = 'V'
        if self.v_spec != 0:
            v_desc += (':' + VPtoStr(self.v_spec))
        tmp.append(v_desc)
        rel = [rel_tostr(e) for e in self.sr_obj]
        tmp.append('%s -> %s' % (self.reObj,' '.join(rel)))
        if self.sr_sub != 0xff:
            tmp.append('srSub: %s' % rel_tostr(self.sr_sub))
        return '\n'.join(tmp)

# the rules are compiled from
# above source, on first call to transform
role_rules = []

# mapping, vprops (in above rules) -> VP_xxx (the value)
vprop_dct =  {'passive':VP_passive,\
    'avgt':VP_avgt, 'ave':VP_ave, 'evt':VP_evt, 'q':VP_isq, \
    'notmodified':VP_notmodified, 'nosubject':VP_nosubject, \
    'isqquery':VP_isqquery}

def make_rules():
    """ get rules for thematic roles """
    def parse_line(li):
        # returns [termIx,termSpec,rel]
        mr = re.match(\
                '(\d)\. ([^)]*) \(([^)]+)',li)
        if mr is not None:
            sp_rel = mr.group(3)
            rel = 0xff if sp_rel == '0xff' else SRids.index(sp_rel)
            return (int(mr.group(1)),mr.group(2),rel)
        mr = re.match('(\d)\. (.*)',li)
        if mr is not None:
            # this assigns "undefined" to sr for the terms
            return (int(mr.group(1)),mr.group(2),0xff)
        assert False
    lines = []
    for li in rules_src.split('\n'):
        li = li.strip()
        if len(li) > 0  and not li.startswith('#'):
            lines.append(li)
    i = 0
    while i < len(lines):
        (term_ix,spec,rel) = parse_line(lines[i])
        assert term_ix == 0
        # Collect source text in "src": this helps debugging
        src = [lines[i]]
        # Line#0 describes the verb
        v_spec = 0
        for e in spec.split(':'):
            e = e.lower()
            if e == 'v':
                continue
            v_spec |= vprop_dct[e]
        rule = RoleRule(len(role_rules),v_spec)
        role_rules.append(rule)
        # collect terms, plus optional final "sub" line
        i += 1
        reTerms = []
        while i < len(lines):
            src.append(lines[i])
            mr = re.match('sub \(([^)]*)',lines[i])
            if mr is not None:
                sp_rel = mr.group(1)
                rule.sr_sub = 0xff if sp_rel == '0xff' else SRids.index(sp_rel)
                # this terminates the current rule
                i += 1
                break
            (term_ix,spec,rel) = parse_line(lines[i])
            if term_ix == 0:
                # this line starts a new rule: current is complete
                break
            reTerms.append(spec)
            rule.sr_obj.append(rel)
            i += 1
        rule.reObj = ' '.join(reTerms)
        rule.src = '\n'.join(src)

def print_rules(fp):
    if fp is None:
        fp = sys.stdout
    for r in role_rules:
        fp.write('%s\n\n' % r.tostr())

def set_roles():
    """
    main entry point for thematic role processing.
    Prior processing has recognized grammatical relations:
    subject terms have been placed on the "agent" list, object
    terms on the "theme" list.
    TODO: expand comment
    """
    def find_rule(e):
        obj_lst = e.rel[SR_theme]
        sublst = e.rel[SR_agent]
        roleRE.set_source(e,obj_lst)
        for r in role_rules:
            if match_verb(e,r.v_spec,sublst) and \
                roleRE.match(obj_lst,r.reObj):
                return r
        return None
    if len(role_rules) == 0:
        make_rules()
    e = pg.eS
    while e != None:
        if e.is_verb():
            r = find_rule(e)
            if r is not None:
                e = apply_rule(e,r)
                continue
        e = e.nxt

def check_vspec(vspec,m):
    return (vspec & m) != 0

def assign_role(v,role,terms):
    """ Assign terms to role in verb "v" """
##    v.rel[role] = terms
##    for ex in terms:
##        ex.sr = role
    for e in terms:
        e.set_scope(v,role)

def resolve_role(e,role):
    """
    resolve a role: "SR_sub" and "SR_obj" are remapped to the value
    (SR_agent, SR_exper, etc.) appopriate for the verb
    """
    if role == SR_sub:
        if e.test_verb_form(VP_evt):
            return SR_exper
        elif e.test_vroot("be"):
            return SR_topic
        else:
            return SR_agent
    elif role == SR_obj:
        if e.test_verb_form(VP_ave):
            return SR_exper
        else:
            return SR_theme
    else:
        return role

def match_verb(v,vspec,sublst):
    """ does verb match the spec associated with a rule? """
    if check_vspec(vspec,VP_isq) and \
        len(v.rel[SR_isqby]) == 0:
        return False
    elif check_vspec(vspec,VP_notmodified) and \
        len(v.rel[SR_modifies]) != 0:
        return False
    elif check_vspec(vspec,VP_nosubject) and \
        len(sublst) != 0:
        return False
    elif check_vspec(vspec,VP_passive) and \
        not v.check_vp(VP_passive):
        return False
    elif check_vspec(vspec,VP_avgt) and \
        not v.test_verb_form(VP_avgt):
        return False
    elif check_vspec(vspec,VP_ave) and \
        not v.test_verb_form(VP_ave):
        return False
    elif check_vspec(vspec,VP_evt) and \
        not v.test_verb_form(VP_evt):
        return False
    elif check_vspec(vspec,VP_isqquery):
        if len(v.rel[SR_isqby]) > 0:
            return v.rel[SR_isqby][0].check_wrd_prop(WP_query)
        return False   
    return True

def apply_rule(e,rule):
    vspec = rule.v_spec
    sr_sub = rule.sr_sub
    # Prior processing has recognized grammatical relations:
    # subject terms have been placed on the "agent" list, object
    # terms on the "theme" list. Save these lists, then clear
    # them.
    sublst = e.rel[SR_agent]
    e.rel[SR_agent] = []
    obj_lst = e.rel[SR_theme]
    e.rel[SR_theme] = []
    for ex in sublst:
        ex.sr = SR_undef
    for ex in obj_lst:
        ex.sr = SR_undef
    # apply the rule
    # object terms
    for i in range(0,len(rule.sr_obj)):
        rel = rule.sr_obj[i]
        role = resolve_role(e,rel)
        if role != 0xff and \
            len(roleRE.match_result[i]) > 0:
            assign_role(e,role,roleRE.match_result[i])
    # subject terms
    role = resolve_role(e,rule.sr_sub)
    if role != 0xff:
        assign_role(e,role,sublst)

    return e.nxt

if __name__ == '__main__':
    make_rules()
    print_rules(None)






