# dev code: this should be moved to lexicon.txt
init = \
"""
# "a long, hollow tube"
DetS Adj Comma Adj Noun|X : reduce(n)

# Multiple modifiers: "very little"
Mod!V+ Adj!V : reduce(adj)
Mod!V+ Adv!V : reduce(adv)

PASS

# "how many" becomes a weakDet|query
# "how cold" becomes a query
_how DetW : reduce(query|detw)
_how Adj|Adv : reduce(query)


# "so many" becomes a weak determinant
# "so cold" becomes a modifier
_so Mod!V* DetW : reduce(detw)
_so Adj!V : reduce(adj)
_so Adv!V : reduce(adv)

# ClausePrep + ClauseMod collapse to a clause prep: "when suddenly"
ClPrep ClMod|Adv : reduce(clprep)

PASS

# "which man", "what reason", etc. 
decl %queryDiscrim : _which|_what
# disambiguate "can" (is Noun and V)
%queryDiscrim _can V : reduce(query,1,2)
%queryDiscrim _can : skip
# the general case: "which man"
%queryDiscrim Noun : reduce(query)

# "many reasons". Adjuticate in favor of the noun.
# But exempt "What can", "where can", etc.
Query _can: skip
DetW Adj? NounV : setpos(x,3,3)

# "the end" : Strong determinant + verb reduces to N (a name)
# Weak determinant + verb is too ambiguous to treat here.
DetS Mod!V* V : reduce(n)

PASS

# POS correction for ambiguous verbs

# "like" as a prep vs. "like" as a verb...
# "will like", "do not like", "I like"
VAdj|Do Mod!V* _like : setpos(verb,3,3)

# "to not like"
_to Mod!V* _like : setpos(verb,3,3)

# "I like"
SubPro _like : setpos(verb,2,2)

# defaults to preposition
_like : setpos(prep|clprep) 

# "a used book"
Det Mod* _used Noun|X : setpos(adj,3,3)

# "John's end"
TickS V!Ger!Past : setpos(noun,2,2)

# "of tears". "qualprep V" in general coerces the verb to X;
# the exception is for gerunds ("tired of waiting for you").
# Also exclude the prep "to"
QualPrep!_to V!Ger : setpos(x,2,2)

PASS

# POS correction for non-verbs

# remark TickS as possessive
TickS Noun : setpos(possessive,1,1)

# "that" as a clause prep
# "I think that (to hope|hoping) is foolish"
V _that _to|V : setpos(clprep,2,2)
# "that is foolish": a weak det
_that V : setpos(detw,1,1)

# remark verbs in non-verb roles
decl %vpBe : Be Mod!V*
decl %vpHave : Have Mod!V*

# "were telling lies", "am setting fire ": $3 is not a
# verb. (But we exclude "I am being eaten by bears")
%vpBe Ger!Be V : setpos(x,3,3)

# "to do tricks"
_to _do V : setpos(x,3,3)

# "(who did you) see last night"
V ModV X : setpos(adj,2,2)

# "The strongest rain (ever) recorded..."
DetS Adj V Adv? V : reduce(n,1,3)

# "by her head", "hit her head"
Prep|V _her N : setpos(adj,2,2)

# "nor do" : reduce to clause-modifier
_nor Do: reduce(clmod,1,2)

"""

leftinit = \
"""
decl %leftV: Mod!V? V
decl %leftN : Det? Mod* X|Noun|N

# "what reason can you think of..."
^Query X|Noun+ %leftV : reduce(query,1,2)

# "in many european countries people visit..."
^Prep Det X X Nexpr %leftV : reduce(n,2,4)

# "European politicians criticize..."
# But exclude "Leon looks shocked"
^X Noun!V %leftV : reduce(n,1,2)
^X X %leftV : reduce(n,1,2)

# "That girl by the door was..."
^ClPrep|ClMod? %leftN QualPrep %leftN %leftV : reduce(n,2,4)

# "(When (suddenly)) a white rabbit with pink eyes ran ..."
^ClPrep|ClMod? %leftN QualPrep %leftN %leftV : reduce(n,2,4)

# "(even) blind people dream": reject "blind" as a verb
^Adv? Adj Noun|X V : setpos(adj,2,2)

"""

vphr = \
"""
decl %vpBe : Be|TickS Adv!V*
decl %vpHave : Have|TickS Adv!V*
decl %vpDo : Do Adv!V*
decl %vpGet : Get Adv!V*
decl %vpAdj : VAdj|TickS Adv!V*
decl %vpV  : Adv!V* V Adv!V*
decl %vpGer  : Adv!V* Ger Adv!V*
decl %vpTickS  : Adv!V* TickS Adv!V*

# "be" forms

# "has been going" -> progressive case
%vpAdj? %vpHave|%vpTickS _been %vpGer : vreduce(past|progressive)

# "has been struck" -> passive case
%vpAdj? %vpHave|%vpTickS _been %vpV : vreduce(past|passive)

# "will|may be going" : mark as progressive
%vpAdj %vpBe %vpGer : vreduce(progressive)

# "will|may be struck" : mark as passive
%vpAdj %vpBe %vpV : vreduce(passive)

# "am being eaten"
%vpBe Mod!V* _being Past : vreduce(passive)

# "being choosen was a surprise"
# This is passive case with no primary theme. Class
# this as a gerund: it will then be parsed as an action.
_not|_never? _being %vpV : vreduce(gerund)

# "was killing" and "was killed"
%vpBe Mod!V? Ger: vreduce(progressive)
%vpBe Mod!V? V: vreduce(passive)

# "have" forms

# "(may|will) have seen : past-perfect
%vpAdj? %vpHave %vpV : vreduce(past|perfect)

# "ought to have seen : past-perfect
%vpAdj _to %vpHave %vpV : vreduce(past|perfect)

# "to have seen" : Class as an infinitive; it will parse as
# an action name.
_to %vpHave %vpV : vreduce(inf)

# do forms
# "do not know"
%vpDo %vpV : vreduce(0)

# "to" forms.

# "have|need to be going", "have|need to get going"
# Class these as subjunctive|progressive
Have|_need _to _be|_get Ger : vreduce(subjunctive|progressive)

# "has to be killed": subjunctive|passive
Have _to _be|_get %vpV : vreduce(subjunctive|passive)

# "(will) have to change": subjunctive|future
# Same classification for "(how) she was to get"
%vpAdj? %vpHave _to %vpV : vreduce(subjunctive|future)
Query Be _to %vpV : vreduce(subjunctive|future,2,4)

# "to get tired": class as passive
_to _get Mod!V? Part: vreduce(passive)

# "used to go": class as past
_used _to Mod!V? V : vreduce(past)

# "to go": standard infinitive
_to Mod!V? Root : vreduce(inf)  

# Misc. forms
# "would go", "will go"
%vpAdj %vpV : vreduce(0)

# "she will"
%vpAdj _not|_never? : vreduce(0)

# final default: collect pre- and post modifiers
Adv!V* V Adv!V* : vreduce(0)

PASS

# Reduce "[was beginning][to understand]
# First verb is marked "VP_prelude".
# VPrelude V :  vreduce(0)

"""

detphr = \
"""
# context dependant cases. Object here is to bind together
# 2 succesive nouns: "a 14th century emporer who said...".
# the strong case...
# DetS DetPhrMod* Noun+ DetPhrDelimRight : reduce(n,1,3)
DetS DetPhrMod|Noun* Noun DetPhrDelimRight : reduce(n,1,3)

# the weak case wants both left and right delims
DetPhrDelimLeft DetW DetPhrMod* Noun+ DetPhrDelimRight : reduce(n,2,4)

# the general cases
# First the strong cases...
DetS+ DetW* DetPhrMod* DetPhrRoot _and|_or DetPhrRoot : reduce(n)
DetS+ DetW* DetPhrMod+ DetPhrRoot : reduce(n)
DetS+ DetW* DetPhrRoot : reduce(n)

PASS

# then the weak cases
DetW+ DetPhrMod* DetPhrRoot _and|_or DetPhrRoot : reduce(n)
DetW+ DetPhrMod+ DetPhrRoot : reduce(n)
DetW+ DetPhrRoot : reduce(n)

# Misc. cases

_no Mod!V* V : reduce(n)
Mod!V!ClMod+ DetPhrRoot] : reduce(n)

# sequences of unknown words: combine 2d term etc. into a single term
# (this is hueristic).
X X X+ : reduce(x,2,3)

PASS
# combine "Nexpr 'of' Nexpr"
Nexpr _of Nexpr : reduce(n)

# "what in hell", "how in the world"
Query _in Nexpr : reduce(n)

# Some verb cases...
# "a large pool of water" ("water" can be a verb)
X|N _of V: reduce(n)

# "in trouble" (reduce to "x", since it can function as mod)
_in V : reduce(x)

PASS

# "John's cat". Previous steps converted TickS to Possessive
# in appropriate contexts.
Nexpr Possessive Nexpr : reduce(n)

# Misc. reductions that occur after det-reduction is complete.
# This could be a distinct reduction phase, but we've tacked
# it on this phase as an additional pass

# "in 1900.."
_by|_in Num  : reduce(clmod,1,2)   

# "Two years ago .."
Nexpr _ago  : reduce(clmod,1,2)

PASS

# "X Nexpr" in initial subject context: reduce to a single Nexpr
# Ex: "European governments criticized ..."
decl %headOpt : [Prep Nexpr]|ClMod|ClPrep
^%headOpt? X Nexpr V : reduce(n,2,3)

# "Nexpr QualPrep Nexpr" in initial subject context: reduce to a single Nexpr
# Ex: "The door in front of Deckard slides open"
^Nexpr QualPrep Nexpr V : reduce(n,1,3)

"""

conj = \
"""
# List of noun: "Alice, Bob, and Carol"
[Nexpr [Comma Nexpr Comma]+ _and|_or Nexpr] : reduce(n)

# List of modifiers
[Mod!V [Comma Mod!V Comma]+ _and|_or Mod!V] | \
[Mod!V [_and|_or Mod!V]+] : reduce(x)

PASS
# Simple noun conjuctions: "Alice and Bob". This is
# context dependant. Major cases are:
# 1. "I saw [[Alice and Bob] leave]"
# 2. "[I kissed Alice] and [Bob left]"
# 3. "[Seeing [ghosts and goblins] is a sign of madness"
decl %NConj : Nexpr _and|_or Nexpr

# Here are the skip cases
# "I kissed Alice and Bob left"
Nexpr V!evt %NConj Mod!V? V: skip

# "I waved to the girl and she waved back"
# "I hit Sally and she hit me back"
Nexpr V!evt Prep? %NConj Mod!V? V : skip

# "I gave him money and he left"
Nexpr V!evt Nexpr %NConj Mod!V? V : skip

# if left term of a conjuction is immediate object
# of a verb or prep, and right term is subject of a verb,
# we skip the conjunction if right term is a subject pronoun.
# Note the left verb is not constrained to be not-evt:
# "I saw the girl and she left"
Nexpr V Prep? Nexpr _and|_or SubPro Mod!V? V : skip

# Accept any remaining cases
%NConj : reduce(x)

"""

parseleft = \
"""

# cases with explicit preps
decl %objTerm : [Prep Nexpr]|Nexpr

# "on the day you left (the ship) we saw mermaids"
Prep|ClPrep QSubVerb %objTerm* AgentAction : setladj(1,2,4)

# "after you left (the ship) we saw mermaids"
Prep|ClPrep AgentAction %objTerm* AgentAction : setladj(1,2,4)

# "on Monday we saw mermaids"
Prep|ClPrep Nexpr AgentAction : setladj(1,2,3)

# "in the last few decades tourism has declined"
Prep|ClPrep Nexpr Nexpr AgentAction : setladj(1,3,4)

# "when suddenly a white rabbit with pink eyes ran..."
# Just mark the ladj relation: subject is expanded later
ClPrep|ClMod Nexpr Prep AgentAction : setladj(1,1,4)

# no explicit prep:
# "the day you left (the ship) we saw mermaids"
QSubVerb %objTerm* AgentAction : setladj(1,1,3)

# clause modifiers: "today we saw mermaids"
ClMod AgentAction : setladj(1,1,2)

"""

