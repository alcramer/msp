# Copyright 2011 Al Cramer

from defs import *
import vcb
import pg
import pgmatch
from pgmatch import match_re,matS,matE,pgRE,decl_re
import rules
import parseReduct

"""
Parse methods for verb domains. A verb domain is a contiguous
sequence of words centered around a verb. To the left are Q
and subject terms. To the right are object terms. There is a
prioriy in binding: subject is strongest, followed by qualification,
followed by is-object-of. At this point in processing Q, sub, and
object relations have been established  and are represented
by SR_isqby, SR_agent, and SR_theme respectively.
"""

# syntax predicates

def is_nvexpr(e):
    """
    is "e" a verb expression that can function as a noun
    ("the girl sitting there", "the boy you saw")
    """
    return e != None and e.check_g(G_nvexpr)

def is_agent_action(e):
    """
    is "e" agent-action? sub-gerund returns false.
    A Q expr ("the girl you saw") will return true, since
    "you saw" is agent-action.
    """
    return e != None and e.check_g(G_agentaction)    

def has_mutable_sub(e):
    """
    is "e" a verb whose subject can be reset?
    """
    return e is not None and not e.check_g(G_subimmutable)

def is_immutable_q(e):
    """
    is "e" a q expression which cannot be changed to subject-verb?
    """
    return e is not None and e.check_g(G_qimmutable)

def is_subord_cl(v):
    """
    Can v function as a subordinate verb clause?
    """
    return v != None and v.check_g(G_subordcl)

# "vdl": verb domain list. It's a list of nodes, linked by vprv and
# vnxt. "v_initial" is the leftmost verb in the list, before any
# reductions are made.

vdl = None
v_initial = None

def vdl_join(left,right):
    if left is not None:
        left.vnxt = right
    if right is not None:
        right.vprv = left

def vdl_remove(v):
    """ 
    remove "v" from the vdl. A no-op if v is not in the list.
    """
    global vdl
    if v == vdl:
        vdl = v.vnxt
        if vdl is not None:
            vdl.vprv = None
    else:
        left = v.vprv
        right = v.vnxt
        vdl_join(left,right)

def vdl_insert(v,e):
    """ insert e in the vdl, immediately before v """
    global vdl
    if v == vdl:
        vdl_join(e,v)
        vdl = e
    else:
        vdl_join(v.vprv,e)
        vdl_join(e,v)

def printvdl(title=None):
    if title != None:
        print title
    e = vdl
    while e is not None:
        e.printme(None,0)
        e = e.vnxt

def prv(e,offset=1):
    """ get predecessor of e in vdl """
    ex = e
    for cnt in range(offset):
        if ex is None:
            break
        ex = ex.vprv
    return ex

def nxt(e,offset=1):
    """ get successor of e in vdl """
    ex = e
    for cnt in range(offset):
        if ex is None:
            break
        ex = ex.vnxt
    return ex

def reduce_prelude(v,e,rel):
    """ add e to v's modifier's list. """
    if isinstance(e,list):
        for ex in e:
            reduce_prelude(v,ex,rel)
        return
    vdl_remove(e)
    e.set_scope(v,rel)

    
def add_obj(v,e):
    """ add e to v's object list. """
    if isinstance(e,list):
        for ex in e:
            add_obj(v,ex)
        return
    vdl_remove(e)
    e.set_scope(v,SR_theme)

def _unreduce(v,ixrel):
    """
    undo a previous reduction: ixrel must be SR_agent or SR_isqby. 
    The unbound nodes are reassigned to the object 
    list of the preceeding verb; or to the vdl, if v is the first 
    verb in the vdl
    """
    assert ixrel in [SR_agent, SR_isqby]
    lst = v.rel[ixrel]
    if len(lst) > 0:
        v.rel[ixrel] = []
        # if this domain has a predecessor domain, its reduced terms
        # are appended to the predeccessor's object list
        if v.vd_left is not None:
            add_obj(v.vd_left,lst)
        else:
            # reinsert the nodes in the vdl, immediately in front
            # of v.
            for e in lst:
                e.unset_scope()
                if v.vd_left == e:
                    vdl_insert(v,e)


def _reduce(v,ixrel,e):
    """
    reduce: ixrel must be SR_agent or SR_isqby
    """
    assert ixrel in [SR_agent, SR_isqby]
    if isinstance(e,list):
        for ex in e:
            _reduce(v,ixrel,ex)
        return
    # undo any previously performed redution. Note that
    # reducing by Subject implicitly undoes any previously created
    # Q relations.
    if ixrel == SR_agent:
        _unreduce(v,SR_isqby)
    _unreduce(v,ixrel)
    e.set_scope(v,ixrel)
    vdl_remove(e)

def reduceS(v,e):
    _reduce(v,SR_agent,e)

def reduce_q(v,e):
    _reduce(v,SR_isqby,e)

def unreduceS(v):
    _unreduce(v,SR_agent)

def unreduce_q(v):
    _unreduce(v,SR_isqby)

def find_pn(lst,mask):
    """ find index in list: test is on WP_xxx props """
    for i in range(len(lst)):
        if lst[i].check_prop(mask):
            return i
    return -1

def match_pn(lst,pat):
    """ match lst against pat: test is on WP_xxx props """
    for i in range(len(pat)):
        if i >= len(lst) or not lst[i].check_prop(pat[i]):
            return False
    return True

def reduce_subobj():
    """
    Reduce subject/objects for verb-domains. Returns False
    if the reduction cannot be performed (the case lies outside
    our model).
    """
    # advance to the first verb
    v = vdl
    if v is None:
        return
    if nxt(v) is None:
        # in general a no-op; but here we correct for incomplete subject
        # phrase analysis.
        # Exclude this case:
        # "a cake good enough to eat"
        # Pattern is "N Adj Inf"; Adj is subject of Inf, and
        # N is qualified by the verb
        if v.check_vp(VP_inf):
            return
        # only merge if verb's current subject is mutable
        if not has_mutable_sub(v):
            return
        # unset any q relation, then assign unscoped terms to
        # subject. Ex.:
        # "The first American moon mission was..."
        for ex in v.rel[SR_isqby]:
            ex.unset_scope()
        # skip initial preps: they become part of header
        ex = pg.eS
        while ex is not None and \
            ex.check_prop(WP_clprep|WP_qualprep|WP_prep):
            ex = ex.nxt
        # assign unscoped terms to subject. Ex.:
        # "The first American moon mission was..."
        while ex is not None and \
            ex != v:
            if ex.scope is None:
                ex.set_scope(v,SR_agent)    
            ex = ex.nxt 
        return

    # get initial scope: either v, or NVexpr + vnxt
    scope = v
    peek = nxt(v)
    if is_nvexpr(v):
        if has_mutable_sub(peek) and not is_immutable_q(peek):
            # General cases:
            # "The girl sitting there was..."
            # "The strongest rain ever recorded in India caused..."
            # v + peek -> SV
            # But exclude:
            # "She wanted me to come"
            if not peek.check_vp(VP_inf):
                reduceS(peek,v)
                scope = peek
    # advance: we're currently in the object context of "scope"
    v = nxt(scope)
    while v is not None:
        peek = nxt(v)
##        pg.printme(None)
##        print scope.h,v.h,peek.h
        if is_nvexpr(v):
            if is_nvexpr(peek):
                # 2 names in an object context. Both becomes object
                # terms of "scope". Scope then shifts to "peek".
                add_obj(scope,v)
                add_obj(scope,peek)
                scope = peek
                v = nxt(scope)
                continue
            if has_mutable_sub(peek) and not is_immutable_q(peek):
                # v + peek ->SV, which is added to scope's object;
                # scope then shifts to peek.
                reduceS(peek,v)
                add_obj(scope,peek)
                scope = peek
                v = nxt(scope)
                continue
            if peek is not None:
                # error (case is outside our model)
                raise ParseErr("could not handle 'peek'")
            # Fall thru to code below
        # "v" added to scope's object list; scope then shifts to v.
        # "peek" becomes the new v. 
        add_obj(scope,v)
        scope = v;
        v = peek
    return

def do_predicate_queries():
    """
    Handle predicate queries. These are queries of the form:
    "is she pretty", "is that man the one you met yesterday",
    etc.: form is verb-subject-object, rather than
    subject-verb-object.
    """
    v0 = vdl
    # get first 2 terms in the object list of v0
    objlst = v0.rel[SR_theme]
    N = len(objlst)
    if N == 0:
        return
    e1 = objlst[0]
    e2 = None if N < 2 else objlst[1]
    # look for the standard vadj case: "is she coming", "was it finished"
    # Want e1 to be a verb with a subject, that doesn't qualify
    # another term.
    if e1 is not None and e1.is_verb() and \
        len(e1.rel[SR_agent]) > 0 and \
        len(e1.rel[SR_isqby]) == 0:
        e1.unset_scope()
        reduce_prelude(e1,v0,SR_vadj)
        return
    # Cases where e1 is NVexpr require analyis..
    if is_nvexpr(e1):
        if e2 is not None and e2.check_vp(VP_gerund):
            # "was the guy you saw today leaving"
            # e1 is subject for e2; v0 is vAdj for e2
            reduceS(e2,e1)
            reduce_prelude(e2,v0,SR_vadj)
            return
        if e2 is not None and is_nvexpr(e2):
            # "is the ring I bought the one you liked?"
            # Accept the current scoping
            pass
        else:
            # "was the guy you saw angry"
            # Here the mod may wind up on e1's object list
            objlst = e1.rel[SR_theme]
            i = find_pn(objlst,WP_mod)
            if i == -1:
                # "is the guy saw here", where "here" is classed as
                # a noun. For want of a better alternative, award
                # all terms in objlst to v0
                i = 0
            for ex in objlst[i:]:
                ex.set_scope(v0,SR_theme)
    # Accept this as an "is" predicate ("is he angry"). First term
    # in object list is the topic
    e1.set_scope(v0,SR_topic)

def do_vadj_queries():
    """
    Handle verb-adjunct queries. These are queries of the form:
    "Did she leave"
    """
    # check object terms: we want subject-verb.
    v0 = vdl
    obj_lst = v0.rel[SR_theme]
    if len(obj_lst) > 0 and \
        len(obj_lst[0].rel[SR_agent]) > 0:
        # objLst[0] is subject-verb, so it's the main
        # verb; v0 is its verb-adjunct
        v_main = obj_lst[0]
        reduce_prelude(v_main,v0,SR_vadj)
        # v0's tense attributes transfer to v_main; excepting
        # some purely syntactic cases, v0 becomes a verb-qualifier
        # for v_main
        v_main.vprops = v0.vprops & VP_tensemask
        if not v0.test_vroot(\
            ['be','have','do','will','shall']):
            v_main.vqual.append(v0.get_wrd(0))
    else:
        # v0 is the main verb.
        v_main = v0
    v_main.unset_scope()

def do_queries():
    """
    Handle query forms: "(why) did he leave...", "is she pretty"
    """
    v0 = v_initial
    if v0 is None:
        return
    # is this a query form? We require leftmost verb be VP_adj; or
    # one of [be,have,do,can]
    if not v0.check_vp(VP_adj):
        if not v0.test_vroot(['be','have','do','can']):
            return
        # Also reject gerund forms
        if v0.check_vp(VP_gerund):
            return
    # If v0 has a subject, in general it's not a query
    # ("she is angry" vs. "is she angry"). Exception is if
    # that subject is query word ("why is she angry").
    qwrd = None
    if len(v0.rel[SR_agent]) > 0:
        sub = v0.rel[SR_agent][0]
        if sub.check_prop(WP_query):
            qwrd = sub
        else:
            # reject as query form
            return
    if v0.test_vroot('be'):
        do_predicate_queries()
    else:
        do_vadj_queries()
    # "v_main" is the root of the parse tree
    if v0.scope is None:
        # "Is she pretty"
        v_main = v0
    else:
        # "Did she leave"
        v_main = v0.scope
    # v0's tense attributes transfer to v_main
    if v_main != v0:
        v_main.vprops &= ~VP_tensemask
        v_main.vprops |= v0.vprops & VP_tensemask
    if qwrd is None:
        # mark v_main as a query verb form
        v_main.set_vp(VP_query)
    else:
        # qwrd is qualified by root of tree: it considered
        # part of the prelude
        reduce_prelude(v_main,qwrd,SR_isqby)
        

def resolve_weak_obj_relations(v):
    # we want verbs with "weak scope": they compete with their
    # parent for object terms. Ex:
    # "I gave the ring I bought in Rome to Sally".
    # Here we decide: is "in Rome" an object of "bought", or "gave"?
    if v.scope is None or \
        not is_nvexpr(v) or \
        v.sr == SR_agent:
        # reject as weak scope
        return v.nxt
    objlst = v.rel[SR_theme]
    owner = v.scope
    prep_mask = WP_prep|WP_clprep|WP_qualprep
    # look for an explicit prep
    for i in range(len(objlst)):
        e = objlst[i]
        if e.check_prop(prep_mask):
            prep = e.get_wrd(0)
            v_fit = vcb.get_prep_verb_fitness(prep,v.get_vroot())
            owner_fit = vcb.get_prep_verb_fitness(prep,owner.get_vroot())
            if owner_fit > 0 and owner_fit > v_fit:
                # this prep, plus terms that follow, are promoted to
                # the owner scope
                terms = objlst[i:]
                for ex in terms:
                    ex.set_scope(owner,SR_theme)
                return v.nxt
    # give "v" a minimal object clause
    def check(i,mask):
	    return i<len(objlst) and objlst[i].check_prop(mask)
    ix = -1
    if match_pn(objlst,[prep_mask,WP_x|WP_n]):
        ix = 2
    elif match_pn(objlst,[WP_x|WP_n]):
        ix = 1
    if ix != -1:
        terms = objlst[ix:]
        for ex in terms:
            ex.set_scope(owner,SR_theme)
    return v.nxt

def set_obj_relations():
    # initial pass: non-verb, un-scoped terms are assigned an
    # object relation to the closest preceeding verb
    scope = None
    ex = pg.eS
    while ex is not None:
        if ex.is_verb():
            scope = ex
        elif ex.scope is None and \
            scope is not None:
            ex.set_scope(scope,SR_theme)
        ex = ex.nxt
    pg.walk(resolve_weak_obj_relations)
    
def bind_left_mods():
    """
    bind unscoped left modifiers: "SOMETIMES we play chess".
    """
    e = pg.eS
    if e.check_prop(WP_mod|WP_clmod) and e.scope is None:
        vroot = vdl
        if vroot is not None and vroot != e:
            reduce_prelude(vroot,e,SR_ladj)
            
parseleft = parseReduct.ParseLeftXfrm('parseleft',rules.parseleft)
def parse():
    """ Main parse method for verb domains. """
    # Create the vdl (verb-domain-list). Structure will be a prelude
    # (non verb nodes), followed by a sequence of verb nodes.
    # Each verb node is actually a verb domain: subject terms
    # are in v.rel[SR_agent], object terms are in v.rel[SR_theme],
    # and qualified terms in v.rel[R_isQby]
    global vdl, v_initial
    vdl = None
    vdl_tail = None
    e = pg.eS
    while e is not None:
        if e.is_verb():
            e.vnxt = e.vprv = None
            if vdl is None:
                vdl = vdl_tail = e
            else:
                vdl_join(vdl_tail,e)
                vdl_tail = e
            # If this verb domain is immediately preceeded by
            # a verb domain, save a reference as "vd_left". This is
            # an invariant: "vprv" and "vnxt" relations change when we
            # do reductions, but "vd_left" is constant. 
            e.vd_left = prv(e)
        e = e.nxt
    # save leftmost term in list
    v_initial = vdl

    # reduce subordinate clauses
    e = vdl
    while e is not None and nxt(e) is not None:
        ex = nxt(e)
        while is_subord_cl(ex):
            add_obj(e,ex)
            ex = nxt(e)
        e = nxt(e)

    # reduce left-adjuncts; adjust vdl if first verb is an adjunct
    parseleft.do_xfrm()
    if vdl is not None and vdl.scope is not None:
        unreduce_q(vdl.scope)
        vdl_remove(vdl)
    
    # reduce subjects and objects for verb-domains
    reduce_subobj()

    # complete object relations
    set_obj_relations()
    
    # Handle query forms: "(why) did he leave...", "is she pretty"
    do_queries()

    # bind unscoped left modifiers ("Sometimes we play chess")
    bind_left_mods()



