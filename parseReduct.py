# Copyright 2012 Al Cramer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pg
from defs import *
import vcb
import serializer
from rematch import pnRE
import pgmatch
import sys

"""
This code performs phrase reductions. At this point the parse graph
consists of a linked list of parse nodes (class "Pn"), representing
words and punctuation. Each node has a "sc" (syntax class) attribute:
this is our generalization of part-of-speach. In reduction, we replace
short sequences of nodes with a single node, so simple phrases like
"the girl" and "didn't go" are parsed as units.

"""
# dev/test
trace_rules = False

def is_vqual(e):
    """ can "e" be a verb-qualifier? """
    return e is not None and \
        e.is_verb() and \
        not e.test_vroot(['be','have','do','will','shall','use'])

def vreduce(S,E,vprops):
    """ reduce a verb phrase, S..E. """
    # get list of verb terms S..E (skip modfiers). Catch negations
    # ("have not seen")
    terms = []
    adverbs = []
    e = S
    while e is not None:
        if e.is_verb() or e.check_prop(WP_ticks):
            terms.append(e)
        elif len(e.wrds) > 0:
            sp = vcb.spell(e.wrds[0]).lower()
            if sp == "not" or sp == "never":
                vprops |= VP_neg
            elif e.check_prop(WP_adv):
                adverbs.extend(e.wrds)
        if e == E:
            break
        e = e.nxt

    # Get additional props (tense, etc.)
    # "vS" and "vE": first/last verb in list
    N = len(terms)
    vS = terms[0]
    vE = terms[N-1]

    # If this is the reduction of an atomic verb phrase, get
    # additional props from vS.
    wp_props = WP_verb
    if len(terms) == 1:
        # all props derived from first term
        mask = VP_formmask|VP_semanticmask|VP_tensemask
        vprops |= (vS.vprops & mask)
        mask = WP_adv|WP_adj|WP_noun
        wp_props |= (vS.props & mask)
    else:
        # in general tense props are taken from first term.
	# Perfect forms ("has seen") is an exception.
	if not vS.test_vroot("have"):
            vprops |= (vS.vprops & VP_tensemask)
        

    # call the graph's reduction method
    R = pg.reduce_terms(S,E,wp_props,vprops)
    # last term gives the root verbs(s)
    R.verbs = vE.verbs[:]
    # save any adverbs
    R.adverbs = adverbs
    # vS and vE gives indices for start and end of verb construct
    R.vS = S.S
    R.vE = E.E
    # some complex forms ("have gone") are purely syntactic; others
    # ("might go") are considered to represent a qualified form for a
    # verb, and we save the qualifier.
    for i in range(0,len(terms)):
        ex = terms[i]
        if len(ex.vqual) > 0:
            R.vqual.extend(ex.vqual)
        if ex != vE and is_vqual(ex):
            R.vqual.append(ex.verbs[0])
    # Reduce "[was beginning][to understand]
    left = R.prv
    if left is not None and \
        left.is_verb() and \
        left.test_verb_form(VP_vpq):
        vprops = R.vprops & VP_semanticmask
        R = vreduce(left,R,vprops)
    return R

class ReductRule:
    """
    A reduction rule. The rule is compiled from "src". Format is:
    RegExpr : action
    where action is
        reduce(props,S,E)
        vreduce(props,S,E)
        setpos(props,S,E)
        skip
        
    "S" and "E" are indices into match terms.If not present, the action
    applies to all terms in the match. If present, it applies only to
    the terms between S and E, inclusive. (This lets us do content sensitive
    matching). The indexing is 1-based rather than 0-based, so "1" refers
    to the frst match term. By convention, "0" means: all terms in the
    match.
    """
    def __init__(self,src):
        self.props = 0
        self.S = 0
        self.E = 0
        self.ix_v = 0
        self.matchHead = False
        self.matchTail = False
        # reg.expr on left of ":", function call on right.
        tmp = src.split(':')
        # tmp[0] is reg.expr. Handle matchHead/Tail qualifiers
        re = tmp[0].strip()
        if re.startswith('^'):
            self.matchHead = True
            re = re[1:].strip()
        if re.endswith('$'):
            self.matchTail = True
            re = re[:-1].strip()
        self.re = re
        fcall = tmp[1].strip().replace(' ','')
        if fcall.startswith('skip'):
            # no args required
            self.act = 'skip'
        else:
            ix = fcall.find('(')
            self.act = fcall[:ix]
            args = fcall[ix+1:-1].split(',')
            if self.act == 'setladj':
                self.S = int(args[0])
                self.E = int(args[1])
                self.ix_v = int(args[2])
            else:
                # arg0 (props list) is required. But "0" means "no props"
                if args[0] != '0':
                    dct = vp_dct if self.act == 'vreduce' else wp_dct
                    for p in args[0].split('|'):
                        self.props |= dct[p.strip()]
                # args 1 and 2 (S and E indices) are optional
                if len(args) > 1:
                    self.S = int(args[1])
                    self.E = int(args[2])
                
    def to_str(self):
        tmp = []
        tmp.append(self.act)
        if self.act == 'vreduce':
            tmp.append('vprops: ' + VPtoStr(self.props))
        elif self.act == 'reduce' or self.act == 'setpos':
            tmp.append('props: ' + WPtoStr(self.props))
        tmp.append('re: "%s"' % self.re)
        if self.S != 0 or self.E != 0:
            tmp.append('S:%d' % self.S)
            tmp.append('E:%d' % self.E)
        if self.ix_v != 0:
            tmp.append('ix_v:%d' % self.ix_v)
        return ' '.join(tmp)

def parse_reduct_rules(src):
    """
    parse a rule set. Returns a list of [rules]; each set of rules
    applies to one pass over the parse graph.
    """
    if len(src) == 0:
        return []
    # kill white space and comments
    lines = []
    for li in src.split('\n'):
        li = li.strip()
        if len(li) > 0 and li[0] != '#':
            lines.append(li)
    # join continued lines
    lines1 = []
    i = 0
    while i < len(lines):
        li = lines[i]
        i += 1
        if not li.endswith('\\'):
            lines1.append(li)
            continue
        tmp = [li[:-1]]
        while i<len(lines) and lines[i].endswith('\\'):
            tmp.append(lines[i][:-1])
            i += 1
        lines1.append(' '.join(tmp))
    lines = lines1
    # get the rules. "rules" is a collection of 1 or more
    # rules-sets, each set containing the rules for a pass
    # over the prase graph.
    rules = []
    cur_set = []
    for li in lines:
        if li.startswith('PASS'):
            # start a new rule set
            if len(cur_set) > 0:
                rules.append(cur_set)    
                cur_set = []
        elif li.startswith("decl"):
            # declare a reg.expr
            li = li[len('decl'):]
            tmp = li.split(':')
            pgmatch.decl_re(tmp[0].strip(),tmp[1].strip())
        else:
            cur_set.append(ReductRule(li))
    # save last set
    if len(cur_set) > 0:
        rules.append(cur_set)
##    # order rules in each set, longest first
##    for i in range(0,len(rules)):
##        tmp = []
##        for r in rules[i]:
##            reterms = r.re.strip().split(' ')
##            tmp.append([len(reterms),r])
##        tmp.sort()
##        tmp.reverse()
##        rules[i] = []
##        for e in tmp:
##            rules[i].append(e[1])
####        for r in rules[i]:
####            print r.to_str()
####        debug1 = 1
    return rules
        
class ReductXfrm():
    """
    Reduction transform. We make several passes over the graph,
    performing different kind of reductions. Each pass is implemented
    by a ReductXfrm.
    """

    def __init__(self,name,src):
	self.name = name
        self.rules = parse_reduct_rules(src)

    def printme(self,fp):
        if fp is None:
            fp = sys.stdout
        fp.write('Xfrm %s\n' % self.name)
        for rs in self.rules:
            fp.write('PASS\n')
            for r in rs:
                fp.write('%s\n' % r.to_str())
        
    def apply_rule(self,r):
        """ 
        apply a reduction rule. Returns node to be tested in next
        match operation.
        """
	global trace_rules
        if trace_rules:
            print "reduct. apply_rule: %s" % r.to_str()
        # the reduction runs from matS(r.S) to matE(r.E), inclusive.
        S = pgmatch.matS(r.S)
        E = pgmatch.matE(r.E)
        if r.act == 'reduce':
            R = pg.reduce_terms(S,E,r.props,0)
            # It's useful to pass up the query prop: "which person".
            if S.check_prop(WP_query):
                R.props |= WP_query
            return R
        if r.act == 'vreduce':
            return vreduce(S,E,r.props).nxt
        if r.act == 'setpos':
            if r.props == WP_verb:
                return vreduce(S,E,0).nxt
            else:
                return pg.reduce_terms(S,E,r.props,0).nxt
        if r.act == 'skip':
            return E.nxt
        assert False, "apply_rule unknown act: %s" % r.act


    def do_xfrm(self):
        for rule_set in self.rules:
            e = pg.eS
            while e != None:
                # is there a rule that matches at "e"?
                r = None
                for rx in rule_set:
                    if rx.matchHead and e != pg.eS:
                        continue
                    if pgmatch.match_re(rx.re,e):
                        r = rx
                        break
                if r is None:
                    # advance to next node
                    e = e.nxt
                else:
                    # apply the rule
                    e = self.apply_rule(r)

class ParseLeftXfrm(ReductXfrm):
    """
    Subclass of ReductXfrm: these actions performed on the
    the left (start) context of a syntax-relations region.
    """

    def __init__(self,name,src):
        ReductXfrm.__init__(self,name,src)
        
    def apply_rule(self,r):
        # print "apply_rule: %s" % r.to_str()
        v = pgmatch.matS(r.ix_v)
        rel = SR_ladj
        for i in range(r.S,r.E+1):
            e = pgmatch.matS(i)
            e.set_scope(v,rel)

    def do_xfrm(self):
        """ Do left (start) context reductions  """
        for rule_set in self.rules:
            # seek a rule that matches at "pg.eS"?
            for rx in rule_set:
                if pgmatch.match_re(rx.re,pg.eS):
                    self.apply_rule(rx)
                    break



