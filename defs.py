# Copyright 2011 Al Cramer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This module defineds constants (mostly bitmasks for various kinds of
properties) used throughout the package.
"""

# trace the parse
trace_parse = False

""" Word properties """
wp_dct = {}
# parts-of-speach
wp_dct['conj'] = WP_conj =      0x1
wp_dct['clprep'] = WP_clprep =    0x2
wp_dct['qualprep'] = WP_qualprep =  0x4
wp_dct['prep'] = WP_prep =      0x8
wp_dct['n'] = WP_n =         0x10
wp_dct['noun'] = WP_noun =      0x20
wp_dct['adj'] = WP_adj =       0x40
wp_dct['sub'] = WP_sub =       0x80
wp_dct['x'] = WP_x =         0x100
wp_dct['verb'] = WP_verb =      0x200
wp_dct['adv'] = WP_adv =       0x400
# "mr","mrs", etc.
wp_dct['abbrev'] = WP_abbrev =    0x800
# "can't"
wp_dct['contraction'] = WP_contraction = 0x1000
# who/what/why/when/where/how
wp_dct['query'] = WP_query =     0x2000
# strong ("a") and weak ("that") determinants
wp_dct['dets'] = WP_dets =      0x4000
wp_dct['detw'] = WP_detw =      0x8000
# punctuation
wp_dct['punct'] = WP_punct =     0x10000
# clause modifiers
wp_dct['clmod'] = WP_clmod =     0x20000
# "'s" lexes as a distinct lexeme, because it's context dependent.
# If it's resolved to be a possessive marker, it's remarked
# "possessive".
wp_dct['ticks'] = WP_ticks =     0x40000
wp_dct['possessive'] = WP_possessive= 0x80000
# A number: "1821"
wp_dct['num'] = WP_num =       0x100000
# Attribution verb for quotation: "he said"
wp_dct['attribution'] = WP_attribution =  0x200000
# A phrase that starts with a string or weak determinant
wp_dct['detphr'] = WP_detphr =  0x400000

# "Modifier" -- either adjective or adverb
wp_dct['mod'] = WP_mod = WP_adv|WP_adj


def WPtoStr(m):
    """ Dump word props """
    s = []
    if (m & WP_conj) != 0: s.append("CONJ")
    if (m & WP_clprep) != 0: s.append("CLPREP")
    if (m & WP_qualprep) != 0: s.append("QUALPREP")
    if (m & WP_prep) != 0: s.append("PREP")
    if (m & WP_n) != 0: s.append("N")
    if (m & WP_noun) != 0: s.append("NOUN")
    if (m & WP_adj) != 0: s.append("ADJ")
    if (m & WP_sub) != 0: s.append("SUB")
    if (m & WP_x) != 0: s.append("X")
    if (m & WP_verb) != 0: s.append("VERB")
    if (m & WP_abbrev) != 0: s.append("ABBREV")
    if (m & WP_contraction) != 0: s.append("CONTRACTION")
    if (m & WP_query) != 0: s.append("QUERY")
    if (m & WP_dets) != 0: s.append("DETS")
    if (m & WP_detw) != 0: s.append("DETW")
    if (m & WP_punct) != 0: s.append("PUNCT")
    if (m & WP_clmod) != 0: s.append("CLMOD")
    if (m & WP_adv) != 0: s.append("ADV")
    if (m & WP_ticks) != 0: s.append("TICKS")
    if (m & WP_possessive) != 0: s.append("POSSESSIVE")
    if (m & WP_num) != 0: s.append("NUM")
    if (m & WP_attribution) != 0: s.append("ATTRIBUTION")
    if (m & WP_detphr) != 0: s.append("DETPHR")
    return '|'.join(s)

""" Verb properties """
vp_dct = {}
vp_dct['neg'] = VP_neg = 0x1
vp_dct['adj'] = VP_adj = 0x2
vp_dct['past'] = VP_past = 0x4
vp_dct['present'] = VP_present = 0x8
vp_dct['future'] = VP_future = 0x10
vp_dct['perfect'] = VP_perfect = 0x20
vp_dct['subjunctive'] = VP_subjunctive = 0x40
vp_dct['inf'] = VP_inf = 0x80
vp_dct['root'] = VP_root = 0x100
vp_dct['gerund'] = VP_gerund = 0x200
vp_dct['passive'] = VP_passive = 0x400
vp_dct['negcontraction'] = VP_negcontraction = 0x800
vp_dct['prelude'] = VP_prelude = 0x1000
vp_dct['vpq'] = VP_vpq = 0x2000
vp_dct['avgt'] = VP_avgt = 0x4000
vp_dct['ave'] = VP_ave = 0x8000
vp_dct['evt'] = VP_evt = 0x10000
vp_dct['isq'] = VP_isq = 0x20000
vp_dct['notmodified'] = VP_notmodified = 0x40000
vp_dct['nosubject'] = VP_nosubject =   0x80000
vp_dct['participle'] = VP_participle =  0x100000
vp_dct['query'] = VP_query = 0x200000
vp_dct['progressive'] = VP_progressive = 0x400000
vp_dct['isqquery'] = VP_isqquery = 0x800000

VP_tensemask = VP_past|VP_present|VP_future|VP_subjunctive
VP_semanticmask = VP_neg|VP_prelude|VP_passive
VP_formmask = VP_inf|VP_gerund|VP_participle|VP_root|VP_passive|VP_progressive

def VPtoStr(m):
    """ Dump verb props """
    s = []
    if (m & VP_neg) != 0: s.append("not")
    if (m & VP_adj) != 0: s.append("adj")
    if (m & VP_past) != 0: s.append("past")
    if (m & VP_present) != 0: s.append("present")
    if (m & VP_future) != 0: s.append("future")
    if (m & VP_perfect) != 0: s.append("perfect")
    if (m & VP_subjunctive) != 0: s.append("subj")
    if (m & VP_inf) != 0: s.append("inf")
    if (m & VP_root) != 0: s.append("root")
    if (m & VP_gerund) != 0: s.append("ger")
    if (m & VP_passive) != 0: s.append("passive")
    if (m & VP_negcontraction) != 0: s.append("NegContraction")
    if (m & VP_prelude) != 0: s.append("prelude")
    if (m & VP_vpq) != 0: s.append("vpq")
    if (m & VP_avgt) != 0: s.append("avgt")
    if (m & VP_ave) != 0: s.append("ave")
    if (m & VP_evt) != 0: s.append("evt")
    if (m & VP_isq) != 0: s.append("isQ")
    if (m & VP_notmodified) != 0: s.append("notModified")
    if (m & VP_nosubject) != 0: s.append("noSubject")
    if (m & VP_participle) != 0: s.append("participle")
    if (m & VP_query) != 0: s.append("query")
    if (m & VP_progressive) != 0: s.append("progressive")
    if (m & VP_isqquery) != 0: s.append("isqquery")
    return '|'.join(s)

""" Syntax-relations """
SR_agent = 0
SR_topic = 1
SR_exper = 2
SR_theme = 3
SR_auxtheme = 4
SR_modifies = 5
SR_isqby = 6
SR_attribution = 7
SR_ladj = 8
SR_vadj = 9
# This used for a word that is in the scope of a verb, but its
# relation is undefined.
SR_undef = 10
# Total number of relations, word->verb
SR_nwordtoverb = 11
# these are computational (not part of the syntax model proper)
SR_sub = 11
SR_obj = 12
SR_head = 13
# names for roles
SRids = ["agent","topic","exper","theme","auxTheme",
    "qual","isqby","attribution","ladj","vadj",
    "undef","sub","obj","head"]

class ParseErr(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

# verb phrase props
G_subverb = 0x1
G_subordcl = 0x2
G_agentaction = 0x4
G_nvexpr = 0x8
G_actname = 0x10
G_qimmutable = 0x20
G_subimmutable = 0x40

def GtoStr(m):
    """ Dump verb props """
    s = []
    if (m & G_subverb) != 0: s.append("subVerb")
    if (m & G_subordcl) != 0: s.append("subordCl")
    if (m & G_agentaction) != 0: s.append("agentAction")
    if (m & G_nvexpr) != 0: s.append("nvExpr")
    if (m & G_actname) != 0: s.append("actName")
    if (m & G_qimmutable) != 0: s.append("qImmutable")
    if (m & G_subimmutable) != 0: s.append("subImmutable")
    return '|'.join(s)
